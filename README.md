# Running Docker builds in Jenkins hosted in Docker

Jenkins (running on Docker) can call up to the host's Docker server to run builds 
that require Docker.

## Running this proof of concept

1. Ensure you have Docker and Docker Compose
1. Run `docker-compose up`
1. Go through the Jenkins setup 
1. Create a job that does something like `sudo docker run --rm hello-world`
1. **MARVEL** as Jenkins runs Docker without Docker being installed in the container

![wow](https://media.giphy.com/media/z4Ut4kYEMW0s8/giphy.gif)

## WELL HOW THE HECK DID YOU DO THAT

I recommend just browsing through the Docker configuration in this project. Here's the gist:

1. Download JUST the docker CLI client. (Do not run dockerd)
1. Share your host's `/var/lib/docker.sock` with the container
1. Either run your container as ROOT (usually advised against) or at least give the jenkins user sudo access to docker CLI

### I N S P I R A T I O N

[the inspiration to not run docker within docker and instead share docker.sock](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/)

[sudoer config](http://container-solutions.com/running-docker-in-jenkins-in-docker/)
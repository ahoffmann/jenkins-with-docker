FROM jenkins/jenkins
USER root
RUN apt-get update && apt-get install -y \
      sudo
COPY sudoers /etc/sudoers
RUN curl https://download.docker.com/linux/static/stable/x86_64/docker-17.06.1-ce.tgz | tar xvz \
      && ln -s /docker/docker /usr/bin/docker
USER jenkins
